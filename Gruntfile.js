module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
  	pkg: grunt.file.readJSON('package.json'),
  	jshint: {
	    all: ['Gruntfile.js', 'src/**/*.js']
	},

  	uglify: {
  		build: {
  			// src: 'src/js/main.js',
  			src: 'build/js/concat.js',

  			dest: 'build/js/main.min.js'
  		}

  	},
  	cssmin: {
  		options: {
  			shorthandCompacting: false,
  			roundingPrecision: -1
  		},
  		target: {
  			files: {
  				'build/css/main.min.css': ['src/css/main.css', 'src/css/second.css']
  			}
  		}
  	},
  	concat: {
  		options: {
  			separator: '\n/* another file start */\n',
  		},
  		dist: {
  			src: ['src/js/main.js', 'src/js/second.js'],
  			dest: 'build/js/concat.js',
  		},
  	},

  	watch: {
  		scripts: {
  			files: ['src/js/*.js', 'src/css/*.css'],
  			tasks: ['default'],
  			options: {
  				spawn: false,
  			},
  		},
  	}

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-csslint');

  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', [ 'jshint', 'concat', 'uglify', 'cssmin']);

};

